void main() {
  int? newNumber; // newNumber type allows nullability
  print(newNumber);  // prints null
  newNumber = 42;   // Update the value of newNumber
  print(newNumber);
}
