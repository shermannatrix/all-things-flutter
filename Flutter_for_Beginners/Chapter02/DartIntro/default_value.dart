sayHappyBirthday(String name, [int age = 21]) {
  return "Happy birthday $name! You are $age years old.";
}

void main() {
  var hello = sayHappyBirthday("Sherman");
}