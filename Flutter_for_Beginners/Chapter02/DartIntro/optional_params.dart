void main() {
  sayHappyBirthday("Sherman");
}

sayHappyBirthday(String name, [int? age]) {
  return "$name is $age years old";
}