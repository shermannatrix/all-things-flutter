void main() {
  int counter = 0;
  while (counter < 10) {
    counter++;
    if (counter == 4) {
      break;
    } else if (counter == 2) {
      continue;
    }
    print(counter);
  }
}