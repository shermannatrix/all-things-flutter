sayHappyBirthday(String name, int age) {
  return "$name is ${age.toString()} years old";
}

void main() {
  sayHappyBirthday("Sherman", 40);
}
