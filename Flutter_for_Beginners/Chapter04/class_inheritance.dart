class Person {
  String? firstName;
  String? lastName;
  Person(this.firstName, this.lastName);
  String get fullName => "$firstName $lastName";
}

class Student extends Person {
  late String nickName = "";
  Student(String firstName, String lastName, this.nickName)
      : super(firstName, lastName);

  @override
  String toString() => "$fullName, aka $nickName";
}

main() {
  Student student = Student("Clark", "Kent", "Kal-El");
  print(student);
}
