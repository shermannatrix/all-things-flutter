class Person {
  String? firstName;
  String? lastName;

  Person(String firstName, String lastName) {
    firstName = firstName;
    lastName = lastName;
  }
  String getFullName() => "$firstName $lastName";

  // Using accessors
  /*String get fullName => "$firstName $lastName";
  String get initials => "${firstName![0]}. ${lastName![0]}.";*/

  /*set fullName(String fullName) {
    var parts = fullName.split(" ");
    firstName = parts.first;
    lastName = parts.last;
  }*/

  // using static fields
  /*static String personLabel = "Person name:";
  static void printsPerson(Person person) {
    print("$personLabel ${person.firstName} ${person.lastName}");
  }*/
}

main() {
  /*Person somePerson = Person();
  somePerson.firstName = "Clark";
  somePerson.lastName = "Kent";
  Person anotherPerson = Person();
  anotherPerson.firstName = "Peter";
  anotherPerson.lastName = "Parker";
  print(somePerson.fullName);
  print(anotherPerson.fullName);
  Person.personLabel = "name:";*/
  // somePerson.fullName = "peter parker";
  Person somePerson = Person("Clark", "Kent");
  print(somePerson.getFullName());
}
